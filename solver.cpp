#include<vector>
#include<string>
#include<iostream>
#include<sstream>
typedef unsigned int uint;
class Board{
    int board_size;
    std::vector<std::vector<int>> row_hints,col_hints;
    std::vector<uint> board_rows,board_cols;
    std::vector<std::vector<int>> row_spans,col_spans;
    int cr,cc;
    bool _isValid(int ofs,std::vector<int> const &spans,std::vector<int> const &hints) const{
        auto nspans{spans.size()},nhints{hints.size()};
        if(spans.back()){
            if(nspans>nhints){
#ifdef DEBUG
                std::cerr << "Too many spans!\n";
#endif
                return false;
            }
            if(spans[nspans-1]>hints[nspans-1]){
#ifdef DEBUG
                std::cerr << "Span too large!\n";
#endif
                return false;
            }
        }else{
            if(nspans>1&&spans[nspans-2]!=hints[nspans-2]){
#ifdef DEBUG
                std::cerr << "Span not matched!\n";
#endif
                return false;
            }
        }
        if(nspans<=nhints){
            auto nslot{board_size-1-ofs};
            auto slot_needed{hints[nspans-1]-spans[nspans-1]};
            for(auto i{nspans}; i<nhints; ++i) slot_needed+=hints[i]+1;
            if(slot_needed>nslot){
#ifdef DEBUG
                std::cerr << "Slot remained: " << nslot << "\n";
                std::cerr << "Slot needed: " << slot_needed << "\n";
                std::cerr << "Remaining slots not enough!\n";
#endif
                return false;
            }
        }
        return true;
    }
    bool isValid(int r,int c) const{
        return _isValid(c,row_spans[r],row_hints[r])&&_isValid(r,col_spans[c],col_hints[c]);
    }
    void _setCell(uint &seq,int ofs,uint v){
        seq=(seq&(~(1<<ofs)))|(v<<ofs);
    }
    void setCell(int r,int c,uint v){
        if(v>0) v=1;
        _setCell(board_rows[r],c,v);
        _setCell(board_cols[c],r,v);
    }
    uint getCell(int r,int c) const{
        if(board_rows[r]&(1<<c)) return 1ul;
        return 0ul;
    }
public:
    Board(int N,std::vector<std::vector<int>> rh,std::vector<std::vector<int>> ch):board_size{N},row_hints{rh},col_hints{ch},cr{0ul},cc{0ul}{
        board_rows=std::vector<uint>(N,0);
        row_spans=std::vector<std::vector<int>>(N,{0});
        board_cols=std::vector<uint>(N,0);
        col_spans=std::vector<std::vector<int>>(N,{0});
    }
    void solve(){
        auto on{0ul};
        while(1){
            if(on){
                if(getCell(cr,cc)){
                    setCell(cr,cc,0);
                    row_spans[cr].back()-=1;
                    if(row_spans[cr].back()!=0) row_spans[cr].push_back(0);
                    col_spans[cc].back()-=1;
                    if(col_spans[cc].back()!=0) col_spans[cc].push_back(0);
                }else{
                    if(cr>0&&getCell(cr-1,cc)){
                        col_spans[cc].pop_back();
                    }
                    if(cc>0&&getCell(cr,cc-1)){
                        row_spans[cr].pop_back();
                    }
                    if(cc==0){
                        cr-=1;
                        cc=board_size-1;
                    }else{
                        cc-=1;
                    }
                    continue;
                }
            }else{
                setCell(cr,cc,1);
                on=1;
                row_spans[cr].back()+=1;
                col_spans[cc].back()+=1;
            }     
#ifdef DEBUG
            std::cerr << "Current cell: " << cr << ", " << cc << "\n";
            showSpans();
            std::cerr << "Board:\n";
            showBoard();
#endif
            if(isValid(cr,cc)){
#ifdef DEBUG
                std::cerr << "Valid board!\n";
#endif
                if(cr==board_size-1&&cc==board_size-1){
                    break;
                }
                on=0;
                if(cc==board_size-1){
                    cr+=1;
                    cc=0;
                }else{
                    cc+=1;
                }
            }
#ifdef DEBUG
            else{
                std::cerr << "Invalid board!\n";
            }
            std::cerr << "\n";
#endif
        }
    }
    void showSpans() const{
        std::cerr << "Columns:\n";
        for(auto const&c:col_spans){
            for(auto a:c){
                std::cerr << a << " ";
            }
            std::cerr << "\n";
        }
        std::cerr << "Rows:\n";
        for(auto const&r:row_spans){
            for(auto a:r){
                std::cerr << a << " ";
            }
            std::cerr << "\n";
        }
    }
    void showBoard() const{
        for(auto i{0}; i<board_size; ++i){
            for(auto j{0}; j<board_size; ++j){
                if(i>cr||(i==cr&&j>cc)){
#ifdef DEBUG
                    std::cerr << "X";
#else
                    std::cout << "X";
#endif
                }else{        
#ifdef DEBUG
                    std::cerr << ((board_rows[i]>>j)&1);
#else
                    std::cout << ((board_rows[i]>>j)&1);
#endif
                }
#ifdef DEBUG
                std::cerr << (j<board_size-1?" ":"\n");
#else
                std::cout << (j<board_size-1?" ":"\n");
#endif
            }
        }
    }
};
int main(int argc,char**argv){
    if(argc<2){
        std::cerr << "Usage: " << argv[0] << " board_size\n";
        return 0;
    }
    auto N{std::stoi(argv[1])};
#ifdef DEBUG
    std::cerr << "Board size: " << N << "\n";
#endif
    auto _s{std::string{}};
    while(std::getline(std::cin,_s)){
#ifdef DEBUG
        std::cerr << _s << "\n";
#else
        std::cout << _s << "\n";
#endif
        auto row_hints{std::vector<std::vector<int>>(N,std::vector<int>{})},
             col_hints{std::vector<std::vector<int>>(N,std::vector<int>{})};
        for(auto n{0}; n<N; ++n){
            std::getline(std::cin,_s);
            auto iss{std::istringstream{_s}};
            for(auto i{0l}; iss>>i;){
                col_hints[n].push_back(i);
            }
        }
        for(auto n{0}; n<N; ++n){
            std::getline(std::cin,_s);
            auto iss{std::istringstream{_s}};
            for(auto i{0l}; iss>>i;){
                row_hints[n].push_back(i);
            }
        }
#ifdef DEBUG
        std::cerr << "Column hints:\n";
        for(auto const &c:col_hints){
            for(auto a:c){
                std::cerr << a << " ";
            }
            std::cerr << "\n";
        }
        std::cerr << "Row hints:\n";
        for(auto const &r:row_hints){
            for(auto a:r){
                std::cerr << a << " ";
            }
            std::cerr << "\n";
        }
        std::cerr << "\n";
#endif
        auto board{Board(N,row_hints,col_hints)};
        board.solve();
        board.showBoard();
    }
}